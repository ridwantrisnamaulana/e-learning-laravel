<?php

namespace App\Exports;

use App\Models\Students;
use Maatwebsite\Excel\Concerns\FromCollection;

class StudentsExport implements FromCollection
{
    protected $tipeFilter;
    protected $valueFilter;

    public function __construct($tipeFilter, $valueFilter) {
        $this->tipeFilter = $tipeFilter;
        $this->valueFilter = $valueFilter;
    }
    public function collection()
    {
        if($this->tipeFilter == null || $this->valueFilter == null) {
            return Students::all();
        } else {
            return Students::search($this->tipeFilter, $this->valueFilter);
        }
    }
}
