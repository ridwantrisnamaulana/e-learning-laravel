<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class students extends Model
{
    protected $table = "students";
    protected $fillable = [
        'id',
        'name',
        'email',
        'address',
        'created_at',
        'updated_at'
    ];
    public static function search($type, $name)
    {
        if ($name == null)
        {
            return null;
        }else{
            if ($type=='name')
            {
                return self::where('name','LIKE',"%{$name}%")->orderBy('created_at', 'DESC')->paginate(10);
            }else if ($type=='email')
            {
                return self::where('email','LIKE',"%{$name}%")->orderBy('created_at', 'DESC')->paginate(10);
            }
        } 
    }
}