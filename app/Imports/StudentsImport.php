<?php

namespace App\Imports;

use App\Models\Students;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class StudentsImport implements ToCollection, WithHeadingRow, ShouldQueue, WithChunkReading
{
    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            Students::create([
                'name'      => $row['name'],
                'email'     => $row['email'],
                'address'   => $row['address'],
            ]); 
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
