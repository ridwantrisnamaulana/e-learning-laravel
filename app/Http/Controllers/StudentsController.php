<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Students;
use App\Http\Controllers\Controller;
use App\Exports\StudentsExport;
use App\Imports\StudentsImport;
use App\Jobs\GeneralImportJob;

use Maatwebsite\Excel\Facades\Excel;
class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if($request->has('key')) 
        {
            $students = Students::search($request->key, $request->keyword);
        }else{
            $students = Students::orderBy('created_at', 'DESC')->paginate(10);
        }
        
        return view('students.index', ['students' => $students]);
    }

    public function example()
    {
        $filename   = 'example.xlsx';
        $path       = public_path('import/'.$filename);
        // dd($path);
        return response()->download($path, $filename, [
            'Content-Type'  =>  'application/vnd.ms-excel',
            'Content-Disposition'  =>  'inline; filename = "' . $filename .'"',
        ]);
    }

    public function exportexcel(Request $request)
    {
        $tipeFilter = $request->get('key');
        $valueFilter = $request->get('keyword');
        return Excel::download(new StudentsExport($tipeFilter, $valueFilter), 'students.xlsx');
    }

    public function importExcel(Request $request)
    {
        $import = new StudentsImport;
        $file = $request->file('file');
        
        $filename = $file->getClientOriginalName();
        $file->move('import', $filename);

        dispatch(new GeneralImportJob($import, public_path('/import/'.$filename))); // running job

        return redirect('students')->with('status', 'Successfully Imported Data');;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = [];
        return view('students.create',compact('students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|min:5',
            'email'     => 'email:rfc',
            'address'   => 'required',
        ]);
        $students = new Students;
        $students->name = $request->name;
        $students->email = $request->email;
        $students->address = $request->address;
        $students->save();

        return redirect('students')->with('status','Successfully Added Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $students = Students::find($id);
        $students->makeHidden(['id']);

        return view('students.show', compact('students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Students::find($id);
        return view('students.edit', compact('students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|min:5',
            'email'     => 'email:rfc',
            'address'   => 'required',
        ]);
        $students = Students::find($id)
        ->update([
        'name' => $request->name,
        'email' => $request->email,
        'address' => $request->address,
        ]);
        return redirect('students')->with('status','Successfully Updated Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = Students::findOrFail($id);
        $students->delete();

        return redirect('students')->with('status','Data Deleted Successfully');
    }
}

