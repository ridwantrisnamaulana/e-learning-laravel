<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StudentsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
    // Routing Page Home
Route::get('/','App\Http\Controllers\HomeController@index')->name('home');
    
    // Routing Page Students
    Route::get('/students','App\Http\Controllers\StudentsController@index');
    Route::get('/students/exportexcel','App\Http\Controllers\StudentsController@exportexcel');
    Route::post('/students/importexcel','App\Http\Controllers\StudentsController@importExcel');
    Route::get('/students/example','App\Http\Controllers\StudentsController@example')->name('example');
    Route::get('/students/create','App\Http\Controllers\StudentsController@create');
    Route::post('/students/store','App\Http\Controllers\StudentsController@store');
    Route::get('/students/edit/{id}','App\Http\Controllers\StudentsController@edit');
    Route::put('/students/update/{id}','App\Http\Controllers\StudentsController@update');
    Route::get('/students/show/{id}','App\Http\Controllers\StudentsController@show');
    Route::delete('/students/destroy/{id}','App\Http\Controllers\StudentsController@destroy');





