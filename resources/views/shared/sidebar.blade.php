<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Main Nagiation</div>
                <a class="nav-link" href="">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div> 
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">Management</div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Data
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link {{ (request()->is('students*')) ? 'active' : ''}}" href="{{url('students')}}"><div class="sb-nav-link-icon"><i class="fas fa-user-graduate"></i></div>
                            Students
                        </a>
                        <a class="nav-link" href="layout-sidenav-light.html">
                        <a class="nav-link" href="{{url('')}}"><div class="sb-nav-link-icon"><i class="fas fa-chalkboard-teacher"></i></div>    
                            Teachers
                        </a>
                        <a class="nav-link" href="layout-sidenav-light.html">
                        <a class="nav-link" href="{{url('')}}"><div class="sb-nav-link-icon"><i class="fas fa-address-card"></i></div>    
                            Class</a>
                        <a class="nav-link" href="layout-sidenav-light.html">
                        <a class="nav-link" href="{{url('')}}"><div class="sb-nav-link-icon"><i class="fas fa-chalkboard"></i></div>    
                            Majors</a>
                    </nav>
                </div>
                <div class="sb-sidenav-menu-heading">Teaching Materials</div>
                <a class="nav-link" href="charts.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Theorys
                </a>
                <a class="nav-link" href="tables.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-align-center"></i></div>
                    Tasks
                </a>
            </div>
        </div>
        {{-- <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            Start Bootstrap
        </div>  --}}
    </nav>
</div>