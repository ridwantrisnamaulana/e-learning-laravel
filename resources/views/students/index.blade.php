@extends('layouts.home')
@section('title')
@section('content')
<div class="card-body">
    <div class="container-fluid">
        <h2 class="mt-4">Data Students</h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Data</li>
              <li class="breadcrumb-item active" aria-current="page">Students</li>
            </ol>
          </nav>
         @if (session('status'))
         <div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{ session('status') }}
		</div>
            
        @endif
        <div class="card-header">
        <a href="{{url('students/exportexcel')}}?key={{Request::get('key')}}&keyword={{Request::get('keyword')}}" class="btn btn-success  @if(empty(Request::get("keyword")) && !empty(Request::get("key"))) disabled @endif" style="margin-bottom: 5px"><i class="fas fa-file-export"></i> Export</a>
            <a href="{{url('')}}" class="btn btn-success" data-toggle="modal" data-target="#exampleModal" style="margin-bottom: 5px"> Import<i class="fas fa-file-import"></i></a>
    <nav class="navbar navbar-light bg-light">
        <a href="{{url('students/create')}}" class="btn btn-md btn-primary" style="margin-bottom: 5px"><i class="fas fa-user-plus"></i> Add Data</a>
            <form action="{{url('/students')}}" method="GET" class="d-flex">
                @csrf
                <div class="form-group" style="width: 75%">
                    <select name="key" id="key" class="form-control" style="width: 100%">
                        <option selected>Select By</option>
                        <option value="name" @if(Request::get("key") == 'name') selected @endif>Name</option>
                        <option value="email" @if(Request::get("key") == 'email') selected @endif>Email</option>
                    </select>
                </div>
                <div class="float-right">
                    <input name="keyword" class="form-control me-2" type="search" placeholder="Search" aria-label="Search" autocomplete="Off">    
                </div>
                <div class="float-right">
                    <button class="btn btn-warning" type="submit"><i class="fas fa-search"></i></button>
                </div>        
            </form>
      </nav>
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <tr>
        <th style="text-align:center">No</th>
        <th style="text-align:center">Name</th>
        <th style="text-align:center">Email</th>
        <th style="text-align:center">Action</th>
    </tr>
    @if($students != null)
    @foreach ($students as $data)
        <tr>
        <td style="text-align:center">{{$loop->iteration}}</td>
        <td style="text-align:center">{{$data->name}}</td>      
        <td style="text-align:center">{{$data->email}}</td>
        <td style="text-align:center">
                <a class="btn btn-success" href="{{ url('students/edit/'.$data->id) }}"><i class="fas fa-user-edit"></i> Update</a>
                |
                <form action="{{ url('students/destroy/'.$data->id) }}" method="POST" class="d-inline" onsubmit="return confirm('Yakin Delete Data?')">
                    @method('Delete')
                    @csrf
                    <button class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button>
                </form>
                |
                <a class="btn btn-info" href="{{ url('students/show/'.$data->id) }}"><i class="fas fa-eye"></i> Detail</a>
        </td>
        </tr>
    @endforeach
    @else 
        <h4 align="center">Data not found</h4>
    @endif
    </table>
    <br>
    @if($students != null)
    {{ $students->links() }}
    @endif
            </div>
        </div>
    </div>
   
    <!-- Modal Import -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="students/importexcel" method="POST" enctype="multipart/form-data">
                    @csrf        
                <div class="modal-body">
                <div class="form-group">
                    <label>File Excel <b class="text-danger">*</b></label>
                    <input class="form-control" type="file" name="file" required="required">
                    <small>Note: <b class="text-danger">*</b> .xlsx</small>
                </div>
                <a href="{{ route('example') }}" class="btn btn-success"><i class="fas fa-download"></i> Example Template</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Done</button>
                <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection