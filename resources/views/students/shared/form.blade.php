
@extends('layouts.home')
@section('title')
@section('content')
    <div class="card-header">
        <strong>Add and Updated Students</strong>
            <div class="float-right">
                <a href="{{ url('students') }}" class="btn btn-warning btn-flat btn-sm">
                <i class="fa fa-undo"></i> Back
                </a>
            </div>
    </div>
    <div class="content-wrapper">
        <br><br><br>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    @if(str_contains(url()->current(), 'students/create'))
                    <form method="POST" action="{{ url('students/store') }}">
                        @csrf 
                    @else
                    <form method="POST" action="{{ url('students/update/'.$students->id) }}">
                        @method('put')
                    @endif
                        @csrf                 
                        <div class="form-group">
                            <label>Name <b class="text-danger">*</b></label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $students->name ?? '' }}" autocomplete="off">
                            @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Email <b class="text-danger">*</b></label>
                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') ?? $students->email ?? '' }}" autocomplete="off">
                            @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Address <b class="text-danger">*</b></label>
                            <textarea name="address" class="form-control @error('address') is-invalid @enderror" value="{{ old('address') }}">{{ old('address') ?? $students->address ?? '' }}</textarea>
                            @error('address')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat" ><i class="fa fa-save"></i> Save</button>
                            <button type="reset" class="btn btn-secondary btn-flat"><i class="fas fa-trash-restore"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection