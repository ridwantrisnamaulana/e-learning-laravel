@extends('layouts.home')
@section('title')
@section('content')
    <!-- Content Detail Students -->
    <div class="card">
        <div class="card-header">
                <strong>Detail Students</strong>
                <div class="float-right">
                <a href="{{ url('students') }}" class="btn btn-warning btn-flat btn-sm">
                    <i class="fa fa-undo"></i> Back
                </a>
                </div>
        </div>
            <div class="card-body table-responsive">
                <div class="row">
                    <div class="col md-8 offset-md-2">
                        <br><br>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th style="width:30%">Name</th>
                                    <td>{{ $students->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $students->email }}</td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td>{{ $students->address }}</td>
                                </tr>
                                <tr>
                                    <th>Created At</th>
                                    <td>{{ $students->created_at }}</td>
                                </tr>
                                <tr>
                                    <th>Updated At</th>
                                    <td>{{ $students->updated_at }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection